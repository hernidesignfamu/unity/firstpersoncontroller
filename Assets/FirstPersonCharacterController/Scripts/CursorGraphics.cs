using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace FirstPersonCharacterController
{
	public class CursorGraphics : MonoBehaviour
	{
		[SerializeField] private CursorManager cursorManager;
		[SerializeField] private Image image;
		[SerializeField] private RectTransform cursorTransform;

		Vector3 targetScale = Vector3.one;
		
		private void Awake()
		{
			Assert.IsNotNull(cursorManager);
			Assert.IsNotNull(image);
			Assert.IsNotNull(cursorTransform);
		}

		private void Update()
		{
			image.color = cursorManager.CursorController.IsSelecting ? cursorManager.selectionColor : Color.white;
			targetScale = Vector3.one * (cursorManager.CursorController.IsSelecting ? cursorManager.selectionScale : 1f);
			cursorTransform.localScale = Vector3.Lerp(cursorTransform.localScale, targetScale, 0.2f * 60f * Time.deltaTime);
		}
	}
}