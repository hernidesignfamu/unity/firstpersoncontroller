using UnityEngine;

namespace FirstPersonCharacterController
{
	public class PickableObject : MonoBehaviour, ICursorInteractable
	{
		public void OnUse()
		{
			gameObject.SetActive(false);
		}
	}
}