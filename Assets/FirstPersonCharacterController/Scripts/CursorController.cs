﻿using UnityEngine;
using UnityEngine.Assertions;

namespace FirstPersonCharacterController
{
    public class CursorController : MonoBehaviour
    {
        [SerializeField] private PlayerInput playerInput;
        [SerializeField] private CursorManager cursorManager;
        [SerializeField] private Transform cameraTransform;
        [SerializeField] private LayerMask layerMask;

        private RaycastHit[] raycastHits = new RaycastHit[10];

        public bool IsSelecting { get; private set; }

        private void Awake()
        {
            Assert.IsNotNull(playerInput);
            Assert.IsNotNull(cursorManager);
            Assert.IsNotNull(cameraTransform);

            cursorManager.RegisterCameraCursorController(this);
        }

        private void Update()
        {
            IsSelecting = false;
            ICursorInteractable cursorInteractable;

            var hitCount = Physics.RaycastNonAlloc(cameraTransform.position, cameraTransform.forward, raycastHits, 10f, layerMask);
            for (int i = 0; i < hitCount; i++)
            {
                // Check if the object is interactable
                cursorInteractable = raycastHits[i].collider.GetComponent<ICursorInteractable>();
                if (cursorInteractable == null)
                    continue;

                // Check the distance only on XZ axis
                var closestPoint = raycastHits[i].collider.ClosestPoint(cameraTransform.position);
                var vectorToTarget = closestPoint - cameraTransform.position;
                vectorToTarget.y = 0;
                
                if(vectorToTarget.magnitude < cursorManager.interactionDistance)
                {
                    IsSelecting = true;
                    
                    if(playerInput.GetButton(PlayerInput.Action.Use))
                        cursorInteractable.OnUse();
                    
                    break;
                }
            }
        }
    }
}
