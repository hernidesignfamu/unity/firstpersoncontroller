using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace FirstPersonCharacterController
{
	public class FirstPersonCharacterMovement : MonoBehaviour
	{
		[SerializeField] private PlayerInput playerInput;
		[SerializeField] private CharacterController characterController;
		[SerializeField] private FirstPersonCharacterSettings characterSettings;
		[SerializeField] private FirstPersonCharacterGroundCheck characterGroundCheck;
		[SerializeField] private Transform ceilingCheck;

		private Vector3 velocity;
		private bool isCrouching;
		private float originalHeight, targetHeight;
		private RaycastHit[] raycastHits = new RaycastHit[5];
		private float speedMultiplier = 1f;
		private float targetSpeedMultiplier;
		private Vector3 fallPosition;
		public bool UseExternalMovement;
		public Vector3 ExternalVelocity;
		public UnityEvent OnJump = new UnityEvent();
		public UnityEvent OnLand = new UnityEvent();

		public float SpeedMultiplier => speedMultiplier;
		public float TargetSpeedMultiplier => targetSpeedMultiplier;
		public bool IsCrouching => isCrouching;

		public event Action<float> Fall; // Fall height

		private void Awake()
		{
			Assert.IsNotNull(characterController);
			Assert.IsNotNull(characterSettings);
			Assert.IsNotNull(characterGroundCheck);
			Assert.IsNotNull(ceilingCheck);

			originalHeight = targetHeight = characterController.height;
		}

		private void Update()
		{
			isCrouching = playerInput.GetButton(PlayerInput.Action.Crouch);


			targetSpeedMultiplier = 1f;

			// Gravity and ceiling
			if (characterGroundCheck.IsGrounded && velocity.y < 0)
				velocity.y = -2f;
			if (characterGroundCheck.IsTouchingCeiling && velocity.y > 0)
				velocity.y = 0;

			// Basic movement
			float moveX = playerInput.GetAxis(PlayerInput.Action.MoveX);
			float moveZ = playerInput.GetAxis(PlayerInput.Action.MoveZ);
			Vector3 move = transform.right * moveX + transform.forward * moveZ;

			if (playerInput.GetButton(PlayerInput.Action.Sprint))
				targetSpeedMultiplier *= 1.5f;

			// Crouching
			targetHeight = isCrouching ? 1f : originalHeight;
			if (isCrouching)
				targetSpeedMultiplier *= 0.5f;
			characterController.height = Mathf.Lerp(characterController.height, targetHeight, 0.2f * 60f * Time.deltaTime);
			characterController.center = new Vector3(0, characterController.height * 0.5f, 0);

			// Jumping
			if (characterGroundCheck.IsGrounded && !isCrouching && playerInput.GetButtonDown(PlayerInput.Action.Jump))
			{
				Jump();
			}

			

			if (UseExternalMovement)
			{
				move = ExternalVelocity;
				characterController.Move(move * Time.deltaTime);
			}
			else
			{
				velocity.y += characterSettings.gravity * Time.deltaTime;
				characterController.Move(velocity * Time.deltaTime);

				// Apply basic movement
				speedMultiplier = Mathf.Lerp(speedMultiplier, targetSpeedMultiplier, 0.2f * 60f * Time.deltaTime);
				characterController.Move(Vector3.ClampMagnitude((move * (characterSettings.moveSpeed * speedMultiplier * 60f) + ExternalVelocity), characterSettings.maxVelocity)* Time.deltaTime);
				// Apply Drag
				ExternalVelocity = Vector3.Lerp(ExternalVelocity, Vector3.zero, (characterGroundCheck.IsGrounded ? characterSettings.groundDrag : characterSettings.airDrag) * Time.deltaTime);
			}

			// Falling event
			if (!characterGroundCheck.WasGrounded && characterGroundCheck.IsGrounded)
			{
				Fall?.Invoke(fallPosition.y - transform.position.y);
				OnLand.Invoke();
			}

			if (!characterGroundCheck.IsGrounded && transform.position.y > fallPosition.y ||
				characterGroundCheck.IsGrounded)
			{
				fallPosition = transform.position;
			}

		}

		public void Jump()
		{
			var maxHeight = characterSettings.jumpHeight;
			var hitCount = Physics.RaycastNonAlloc(ceilingCheck.position, ceilingCheck.up, raycastHits,
				characterSettings.jumpHeight, characterGroundCheck.GroundMask);
			if (hitCount > 0)
				maxHeight = raycastHits[0].point.y - ceilingCheck.position.y;

			velocity.y = Mathf.Sqrt(maxHeight * -2f * characterSettings.gravity);
			OnJump.Invoke();
		}
	}
}
