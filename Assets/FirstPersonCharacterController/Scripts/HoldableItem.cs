using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FirstPersonCharacterController
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    public class HoldableItem : MonoBehaviour, ICursorInteractable
    {   
        public bool IsHeld { get; private set; }

        private Collider myCollider;
        private Rigidbody myRb;
        private int myDefaultLayer;
        private Renderer[] allMyRenderers;
        private PlayerHand playerHand;
        private float dropForce = 2;

        protected virtual void Start()
        {
            myDefaultLayer = gameObject.layer;
            allMyRenderers = GetComponentsInChildren<Renderer>();
            myCollider = GetComponent<Collider>();
            myRb = GetComponent<Rigidbody>();
            playerHand = FindObjectOfType<PlayerHand>();
        }

        public void OnUse()
        {
            playerHand.TryPickUp(this);
        }

        // Attach this item to player hand
        public virtual void PickUp(string inHandLayerName)
        {
            transform.SetParent(playerHand.transform);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            myRb.isKinematic = true;
            myCollider.isTrigger = true;

            myDefaultLayer = gameObject.layer;
            gameObject.layer = LayerMask.NameToLayer(inHandLayerName);
            foreach (var rndr in allMyRenderers)
            {
                rndr.gameObject.layer = LayerMask.NameToLayer(inHandLayerName);
            }

            IsHeld = true;
        }

        // Detach this item from player hand and drop it
        public virtual void Drop()
        {
            transform.SetParent(null);

            myRb.isKinematic = false;
            myCollider.isTrigger = false;
            myRb.AddForce(playerHand.transform.forward * dropForce, ForceMode.Impulse);
            myRb.AddForce(playerHand.transform.up * dropForce, ForceMode.Impulse);

            gameObject.layer = myDefaultLayer;
            foreach (var rndr in allMyRenderers)
            {
                rndr.gameObject.layer = myDefaultLayer;
            }

            IsHeld = false;
        }

    }
}
