using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace FirstPersonCharacterController
{
    [RequireComponent(typeof(Rigidbody))]
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float damage;
        [SerializeField] private bool useGravity;
        [SerializeField] private Rigidbody rb;

        private float destroyTimer;
        private float lifetime = 2f;

        private void Start()
        {
            Assert.IsNotNull(rb);
            rb.useGravity = useGravity;
        }

        private void OnEnable()
        {
            destroyTimer = lifetime;
        }

        private void OnCollisionEnter(Collision collision)
        {
            var hitObject = collision.gameObject.GetComponent<DamagableObject>();
            if (hitObject != null)
            {
                hitObject.DamageSelf(damage);
            }

            DestroySelf();
        }

        private void Update()
        {
            destroyTimer -= Time.deltaTime;
            if (destroyTimer <= 0)
            {
                DestroySelf();
            }
        }

        private void DestroySelf()
        {
            rb.velocity = Vector3.zero;
            gameObject.SetActive(false);
        }
    }
}
